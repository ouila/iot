import bluetooth
import json
import os
import requests

def merge_two_dicts(x, y):
    dat = x.copy()
    dat.update(y)
    return dat

def detect(sec):
    print("performing inquiry...")
    nearby_devices = bluetooth.discover_devices(duration=sec, lookup_names=True, flush_cache=True, lookup_class=False)
    print("found %d devices" % len(nearby_devices))
    x = dict()
    z = dict()
    for addr, name in nearby_devices:
        try:
            x = {name : addr}
            z = merge_two_dicts(x, z)
        except UnicodeEncodeError:
            print("  %s - %s" % (addr, name.encode('utf-8', 'replace')))
    return z

def POST(token, data): # Bryan is present
    headers = {'Content-Type':'application/json','Authorization':'Bearer '+token}
    r = requests.put("http://192.168.1.43:8000/api/presentBluetooth", headers=headers ,data=data)
    return r.text

def login(): 
    headers = {'Content-Type':'application/json'}
    data = json.dumps({"email":"raspberrypi@pi.com","password":"raspberrypi"})
    r = requests.post('http://192.168.1.43:8000/api/login', headers=headers, data=data)
    data = r.json()
    return data['success']['token']


data1 = detect(5)
data = json.dumps({"mac_address":[{"mac":"48:2C:A0:A8:C8:56"},{"mac":"48:2C:A0:A8:C8:55"}]})
token = login()
post = POST(token, data)
print(post)







# z = detect(5)
# print z