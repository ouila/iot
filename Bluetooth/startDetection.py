import bluetooth
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
import json
import time
import os

class Server(BaseHTTPRequestHandler):

    def do_POST(self):
        self.send_response(200)
        self.send_header('Content-Type','application/json')
        self.end_headers()
        if self.path == "/bluetooth_on":
            self.wfile.write(json.dumps({'Bluetooth': 'On'}))
            os.system('python MyDiscoverer.py')
        else:
            self.wfile.write(json.dumps({'error': 'true'}))

def run(server_class=HTTPServer, handler_class=Server, port=6001):
    server_address = ('192.168.1.50',port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv
        
    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()